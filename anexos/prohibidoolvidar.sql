create database prohibidoolvidar;
use prohibidoolvidar;

create table prohibidoolvidar(
id_pro bigint not null primary key auto_increment,
id bigint not null,
is_retweet bigint not null,
yes_vote bigint not null,
no_vote bigint not null,
full_text text
);

select * from prohibidoolvidar;

