package com.sv.prohibidoOlvidarSV.utils;


import java.util.List;

/* mi interface que hace referencia a mi clase abstracta - se utiliza patron Abstract Facade */


public interface AbsMemoria<T> {
	
	public List <T> gets();
	
	public T getById(Long id);
	
	public List <T> getByNext();
	
	public List <T> getByTwenty();
	
	public T registry(T r);
	
	public void clean(T e);

	
	

}
