package com.sv.prohibidoOlvidarSV.utils;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sv.prohibidoOlvidarSV.beans.Memoria;

/**  interface que se comunicara tanto con los modulos de Spring como con el modulo
	de la persistencia (interfaz de contrato - interfaz de control) */

@Repository
public interface MemoriaRepository extends JpaRepository<Memoria, Long>{
	

}
