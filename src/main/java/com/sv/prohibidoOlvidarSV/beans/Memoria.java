package com.sv.prohibidoOlvidarSV.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/** Entidad de la cual crearemos la persistencia y de donde se consumiran nuestros datos */

@Entity
@Table(name="prohibidoolvidar")
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class Memoria {
	
	private long id_pro;
	private long id;
	private long is_retweet;
	private long yes_vote;
	private long no_vote;
	private String full_text;
	
	public Memoria() {
		
	}

	public Memoria(long id_pro, long id, long is_retweet, long yes_vote, long no_vote, String full_text) {
		this.id_pro = id_pro;
		this.id = id;
		this.is_retweet = is_retweet;
		this.yes_vote = yes_vote;
		this.no_vote = no_vote;
		this.full_text = full_text;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_pro")
	public long getId_pro() {
		return id_pro;
	}

	public void setId_pro(long id_pro) {
		this.id_pro = id_pro;
	}

	@Column(name="id")
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name="is_retweet")
	public long getIs_retweet() {
		return is_retweet;
	}

	public void setIs_retweet(long is_retweet) {
		this.is_retweet = is_retweet;
	}

	@Column(name="yes_vote")
	public long getYes_vote() {
		return yes_vote;
	}

	public void setYes_vote(long yes_vote) {
		this.yes_vote = yes_vote;
	}

	@Column(name="no_vote")
	public long getNo_vote() {
		return no_vote;
	}

	public void setNo_vote(long no_vote) {
		this.no_vote = no_vote;
	}

	@Column(name="full_text")
	public String getFull_text() {
		return full_text;
	}

	public void setFull_text(String full_text) {
		this.full_text = full_text;
	}
	
	
	
	
	

}
