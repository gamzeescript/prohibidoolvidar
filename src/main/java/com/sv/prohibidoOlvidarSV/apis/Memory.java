package com.sv.prohibidoOlvidarSV.apis;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sv.prohibidoOlvidarSV.beans.Memoria;
import com.sv.prohibidoOlvidarSV.utils.AbsMemoria;

/** Clase controladora que administra las peticiones que vendran de nuestro cliente y las procesara entre nuestros modulos */

@RestController 
@RequestMapping("/apis")
public class Memory {
	
	
		@Autowired
		AbsMemoria mem;

		@GetMapping("/memento")
		public List<Memoria> getAll(){ 

		return mem.gets();

		 }

		 @GetMapping("/memento/{id}")
		 public ResponseEntity<Memoria> getById(@PathVariable(value = "id")Long id) { 
		 	Memoria mr = (Memoria) mem.getById(id);
		 	return ResponseEntity.ok().body(mr);


		 }
		 
		 @GetMapping("/memento/nextTweet")
		 public List<Memoria> getSiguiente() { 
		 	return mem.getByNext();


		 }
		 
		 @GetMapping("/memento/firstTwenty")
		 public List<Memoria> getPrimerosVeinte() { 
		 	return  mem.getByTwenty();


		 }



		 @PostMapping("/memento/storeTweet")
		 public Memoria store(@RequestBody Memoria memor){ 

		 return (Memoria) mem.registry(memor);

		  }

		  @PutMapping("/memento/{id}")
		  public ResponseEntity<Memoria> update(@PathVariable(value = "id")Long id, @RequestBody Memoria memor){

		  Memoria registro = (Memoria) mem.getById(id);
		  registro.setId(memor.getId());
		  registro.setIs_retweet(memor.getIs_retweet());
		  registro.setYes_vote(memor.getYes_vote());
		  registro.setNo_vote(memor.getNo_vote());
		  registro.setFull_text(memor.getFull_text());
		  final Memoria actual = (Memoria) mem.registry(registro);
		  return ResponseEntity.ok(actual);


		   }

		   @DeleteMapping("/memento/{id}")
		   public Map<String, Boolean> delete(@PathVariable(value = "id")Long id){

		   Memoria quitar = (Memoria) mem.getById(id);
		   mem.clean(quitar);
		   Map<String, Boolean> response = new HashMap<String, Boolean>();
		   response.put("borrado exitoso", true);
		   return response;

	}
		   
	}


